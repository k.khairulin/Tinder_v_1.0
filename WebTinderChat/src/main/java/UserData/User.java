package UserData;

import java.util.ArrayList;
import java.util.List;

public class User {
    public String userName;
    public String url;
    public int id;
    public int gender;

    public User(){

    }
    public User(String userName, String url, int id, int gender){
        this.id = id;
        this.userName = userName;
        this.gender = gender;
        this.url = url;
    }
}
