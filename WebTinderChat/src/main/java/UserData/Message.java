package UserData;

public class Message {
    public String message;
    public String time;
    public int sender;
    public int receiver;

    public Message(String message, String time, int sender, int receiver){
        this.message = message;
        this.time = time;
        this.sender = sender;
        this.receiver = receiver;
    }
}

