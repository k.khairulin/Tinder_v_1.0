package ServletHandler;

import DAO.Database;
import DAO.UsersDAO;
import UserData.Message;
import UserData.User;

import javax.servlet.http.HttpSession;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.sql.DriverManager.getConnection;

public class ServletHandler {
    public ServletHandler(){

    }
    public static void like(User thisUser, User curUser) throws SQLException{

        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        ArrayList<String> values = new ArrayList<>();
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        stm.executeUpdate("INSERT INTO Liked(USER1, USER2) VALUES (" + thisUser.id + "," + curUser.id + ")");
    }

    public static void addMessage(User sender, User receiver, String text) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        stm.executeUpdate("INSERT INTO Messages(SENDER_ID, RECEIVER_ID, TEXT)" +
                " VALUES (" + sender.id + "," + receiver.id + ",\"" + text + "\")");
    }

    public static void setChatter(User user, int id) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        stm.executeUpdate("UPDATE Users SET" +
                " CHATTER =" + id + "   WHERE USERS_ID=" + user.id);
    }

    public static User getChatter(User user) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery("SELECT * FROM Users WHERE USERS_ID =" + user.id);
        int id = 0;
        while (rs.next()) {
            id = rs.getInt("CHATTER");
        }
        rs.close();
        stm.close();
        con.close();
        User user1 = UsersDAO.getUserById(id);
        return user1;
    }

    public static ArrayList<Message> getMessageHistory(User sender, User receiver) throws SQLException{
        boolean check = false;
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ArrayList<Message> messageHistory = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm:ss");
        String query = "SELECT * FROM Messages WHERE SENDER_ID IN (" + sender.id + ")" +
                " AND RECEIVER_ID IN (" + receiver.id + ")";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery(query);

        while (rs.next()) {
            String time = sdf.format(rs.getTimestamp("TIME"));
            messageHistory.add(new Message(rs.getString("TEXT"), time, sender.id, receiver.id));
        }
        return messageHistory;
    }

    public static boolean checkIfLiked(User thisUser, User curUser) throws SQLException{
        boolean check = false;
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery("SELECT USER2 FROM Liked WHERE USER1 IN (" + thisUser.id + ")");
        while (rs.next()) {
            int id = rs.getInt("USER2");
            if(id == curUser.id){
                check = true;
            }
        }
        rs.close();
        stm.close();
        con.close();
        return check;
    }
}
