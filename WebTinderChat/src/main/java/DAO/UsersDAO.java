package DAO;

import ServletHandler.ServletHandler;
import UserData.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersDAO {

    public static User getRandomUser(User thisUser){
        String query = "SELECT * FROM Users ORDER BY RAND() LIMIT 1";
        User user = new User();
        try {
            user = Database.queryUser(query);
            while (user.id == thisUser.id || ServletHandler.checkIfLiked(thisUser, user) ||
                    user.gender == thisUser.gender){
                user = Database.queryUser(query);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return user;
    }

    public static ArrayList<User> getMatches(User thisUser){
        ArrayList<User> liked = getLikedUsers(thisUser);
        ArrayList<User> matches = new ArrayList<>();
        for (User curUser : liked) {
            try {
                if (ServletHandler.checkIfLiked(curUser, thisUser)) {
                    matches.add(curUser);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return matches;
    }

    public static User getUserById(int id){
        String query = "SELECT * FROM USERS WHERE USERS_ID IN(\"" + id +"\")";
        User user = new User();
        try {
            user = Database.queryUser(query);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return user;
    }

    public static ArrayList<User> getLikedUsers(User thisUser){
        String query = "SELECT USER2 FROM Liked WHERE USER1 IN (" + thisUser.id + ")";
        ArrayList<Integer> likedUsersId = new ArrayList<>();
        ArrayList<User> likedUsers = new ArrayList<>();
        try {
            likedUsersId = Database.getUsersId(query);
        }catch (SQLException e){
            e.printStackTrace();
        }
        for (int id : likedUsersId) {
            likedUsers.add(UsersDAO.getUserById(id));
        }
        return likedUsers;
    }



}
