package DAO;



import UserData.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static java.sql.DriverManager.getConnection;
import static java.sql.DriverManager.getDrivers;

public class Database {
    public static String makeOneQuery(String query, String field) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        String str = "";
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery(query);
        while (rs.next()) {
            str = rs.getString(field);
        }
        rs.close();
        stm.close();
        con.close();
        return str;
    }

    public static int getSize(String field) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        int size = 0;
        String query = "SELECT COUNT(" + field + ") FROM Users";
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery(query);
        while (rs.next()) {
             size = rs.getInt(field);
        }
        rs.close();
        stm.close();
        con.close();
        return size;
    }

    public static ArrayList<String> makeQuery(String query, String field) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        ArrayList<String> values = new ArrayList<>();
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery(query);
        while (rs.next()) {
            values.add(rs.getString(field));
        }
        return values;
    }

    public static User queryUser(String query) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery(query);
        User user = new User();
        while (rs.next()) {
            user = new User(rs.getString("NAME"), rs.getString("URL"),
                    rs.getInt("USERS_ID"), rs.getInt("GENDER"));
        }
        return user;

    }

    public static ArrayList<Integer> getUsersId(String query) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        ArrayList<Integer> usersId = new ArrayList<>();
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();
        rs = stm.executeQuery(query);
        while (rs.next()) {
            usersId.add(rs.getInt("USER2"));
        }
        rs.close();
        stm.close();
        con.close();
        return usersId;
    }

    public static void main(String[] args) throws SQLException{
        String PASSWD = "rjcnz1405";
        String DB_URL = "jdbc:mysql://localhost:3306/Tinder?autoReconnect=true&useSSL=false";
        String USER = "root";
        ResultSet rs;
        Connection con = getConnection(DB_URL, USER, PASSWD);
        Statement stm = con.createStatement();



    }

}
