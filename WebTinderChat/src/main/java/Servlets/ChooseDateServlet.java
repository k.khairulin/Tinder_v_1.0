package Servlets;
import DAO.UsersDAO;
import ServletHandler.ServletHandler;
import UserData.User;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.servlet.*;

import javax.servlet.http.HttpServlet;

import static java.sql.DriverManager.getConnection;

@WebServlet(urlPatterns = "/liked")
public class ChooseDateServlet  extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, java.io.IOException {
        HttpSession session=req.getSession(true);
        String id = (String) session.getAttribute("id");
        if(id == "" || id == null){
            resp.sendRedirect("/");
            return;
        }
        User thisUser = UsersDAO.getUserById(Integer.parseInt(id));
        ArrayList<User> matches = UsersDAO.getMatches(thisUser);

        PrintWriter out = resp.getWriter();
        out.write("<!DOCTYPE html>\n" +
                "<html lang=\"en\" xmlns:localhost=\"http://www.w3.org/1999/xhtml\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Choose your Match</title>\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                "<style>\n" +
                        "        .par{\n" +
                        "            height: 170px;\n" +
                        "        }\n" +
                "                .image{\n" +
                "                    border: 2px solid black;\n" +
                "                }\n" +
                                ".button1{\n" +
                "                    width: 140px;" +
                "                    height: 35px;" +
                "                    font-size: 160%;" +
                "                }\n" +
                "</style>\n" +
                "</head>\n" +
                "<body background=\"https://d1p42fqrbwqdsw.cloudfront.net/campaigns/background_images/000/015/207/web/MallzeeTwitterBackground.jpg?1408197264\">" +
                "<form action=\"/liked\" method=\"POST\">");
        for (int i = 0; i < matches.size(); i++) {
            String userName = matches.get(i).userName;
            String picture = matches.get(i).url;
            int curId = matches.get(i).id;
            out.write(String.format("<p class=\"par\">" +
                    "<button class=\"button1\" name=\"openchat\" type=\"submit\"value=\"%d\">%s</button>" +
                    "<br>" +
                    "<img class=\"image\" height=\"140px\" width=\"140px\" src=\"%s\">" +
                    "</p>\n", curId, userName, picture));
        }

        out.write("</form> " + "\n</body>\n" +
                "</html>");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session=req.getSession(true);
        String id = (String) session.getAttribute("id");
        if(id == "" || id == null){
            resp.sendRedirect("/");
            return;
        }   
        User thisUser = UsersDAO.getUserById(Integer.parseInt(id));
        int chatterId = Integer.parseInt(req.getParameter("openchat"));
        try {
            ServletHandler.setChatter(thisUser, chatterId);
        }catch (SQLException e){
            e.printStackTrace();
        }
        resp.sendRedirect("/chat");

    }
}


