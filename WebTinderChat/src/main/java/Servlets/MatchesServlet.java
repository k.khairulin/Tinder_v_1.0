package Servlets;

import DAO.Database;
import DAO.UsersDAO;
import ServletHandler.ServletHandler;
import UserData.User;
import com.google.common.base.Charsets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/match")
public class MatchesServlet extends HttpServlet{

  int userId = 0;
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws
      ServletException, IOException {
    HttpSession session=req.getSession(true);
    String id = (String) session.getAttribute("id");
    if(id == "" || id == null){
      resp.sendRedirect("/");
      return;
    }

    File file = new File("src/main/webapp/WEB-INF/match.html");
    String format = com.google.common.io.Files.toString(file, Charsets.UTF_8);

    User thisUser = UsersDAO.getUserById(Integer.parseInt(id));
    User curUser = UsersDAO.getRandomUser(thisUser);
    userId = curUser.id;

    String urlPhoto = curUser.url;
    String userName = curUser.userName;

    resp.getWriter().write(String.format(format, urlPhoto, userName));

  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
    HttpSession session=req.getSession(true);
    String id = (String) session.getAttribute("id");
    User thisUser = UsersDAO.getUserById(Integer.parseInt(id));
    User curUser = UsersDAO.getUserById(userId);
    String button = req.getParameter("button");

    if("yesbutton".equals(button)) {
      try {
        ServletHandler.like(thisUser, curUser);
      }catch (SQLException e){
        e.printStackTrace();
      }
      resp.sendRedirect("/match");

    }else if("nobutton".equals(button)){
      resp.sendRedirect("/match");
    }
  }
}
