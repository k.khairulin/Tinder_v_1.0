package Servlets;

import DAO.UsersDAO;
import ServletHandler.ServletHandler;
import UserData.Message;
import UserData.User;
import com.google.common.base.Charsets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*<div class="container">
<img src="%s" alt="Avatar">
<p>%s</p>
<span class="time-right">%s</span>
</div>

<div class="container darker">
<img src="%s" alt="Avatar" class="right">
<p>%s</p>
<span class="time-left">%s</span>
</div>*/

@WebServlet(urlPatterns = "/match")
public class ChatServlet extends HttpServlet{

    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, java.io.IOException {
        HttpSession session=req.getSession(true);
        String id = (String) session.getAttribute("id");
        if(id == "" || id == null){
            resp.sendRedirect("/");
            return;
        }
        User thisUser = UsersDAO.getUserById(Integer.parseInt(id));
        User chatter = new User();
        try {
            chatter = ServletHandler.getChatter(thisUser);
        }catch (SQLException e){
            e.printStackTrace();
        }
        ArrayList<Message> messageHistory = new ArrayList<>();
        ArrayList<Message> messagesByChatter = new ArrayList<>();

        try {
            messageHistory = ServletHandler.getMessageHistory(thisUser, chatter);
            messagesByChatter = ServletHandler.getMessageHistory(chatter, thisUser);
            for (Message message : messagesByChatter){
                messageHistory.add(message);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        messageHistory.sort((m1, m2) -> m1.time.compareTo(m2.time));

        File file = new File("src/main/webapp/WEB-INF/chat.html");
        PrintWriter out = resp.getWriter();
        String format = com.google.common.io.Files.toString(file, Charsets.UTF_8);

        String userPhoto = thisUser.url;
        String urlPhoto = chatter.url;
        String userName = chatter.userName;
        String html = "";

        for (Message message : messageHistory) {
            String time = message.time.substring(11);
            String text = message.message;
            int sender = message.sender;
            int receiver = message.receiver;
            if(sender == thisUser.id) {
                html += "<div class=\"container\">\n" +
                        "<img src=\"" + userPhoto + "\" " +
                        "alt=\"Avatar\">\n" +
                        "<p>" + "<b>Me: </b>" + text +
                        "</p>\n" +
                        "<span class=\"time-right\">" + time +
                        "</span>\n" +
                        "</div>\n";
            }else {
                html += "<div class=\"container darker\">\n" +
                        "<img src=\"" + urlPhoto + "\" alt=\"Avatar\" class=\"right\">\n" +
                        "<p><b>" + userName + ": " + "</b>" + text +
                        "</p>\n" +
                        "<span class=\"time-left\">" + time + "</span>\n" +
                        "</div>\n";
            }
        }



        resp.getWriter().write((String.format(format, html)));

    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session=req.getSession(true);
        String id = (String) session.getAttribute("id");
        User thisUser = UsersDAO.getUserById(Integer.parseInt(id));
        User chatter = new User();
        try {
            chatter = ServletHandler.getChatter(thisUser);
        }catch (SQLException e){
            e.printStackTrace();
        }
        String message = req.getParameter("message");
        try {
            ServletHandler.addMessage(thisUser, chatter, message);
        }catch (SQLException e){
            e.printStackTrace();
        }

        resp.sendRedirect("/chat");

    }
}
