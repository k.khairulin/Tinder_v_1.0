package Servlets;

import DAO.Database;
import com.google.common.base.Charsets;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name = "login", loadOnStartup = 1, urlPatterns = "/")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        File file = new File("src/main/webapp/WEB-INF/login.html");
        String format = com.google.common.io.Files.toString(file, Charsets.UTF_8);
        resp.getWriter().write(format);
    }

    public static Map<String, String> tokens = new HashMap<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        boolean known = false;
        String userName = req.getParameter("username");
        String password = req.getParameter("password");
        String check = "";
        try{
            check = Database.makeOneQuery("SELECT `PASSWORD` FROM Users WHERE EMAIL LIKE '" + userName + "'","PASSWORD");
        }catch (SQLException e){
            e.printStackTrace();
        }
        if(check.equals(password)){
            known = true;
        }

        String id = "";

        try{
            id = Database.makeOneQuery("SELECT `USERS_ID` FROM Users WHERE EMAIL LIKE '" + userName + "'","USERS_ID");
        }catch (SQLException e){
            e.printStackTrace();
        }
        /*String token = UUID.randomUUID().toString();
        tokens.put(token, userName);*/
        HttpSession session=req.getSession();
        session.setAttribute("id", id);
        /*resp.addCookie(new Cookie("user-token", token));*/

        if(known){
            resp.sendRedirect("/match");
        }else{
            resp.sendRedirect("/");
        }
    }
}